// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterBulletManager.h"
#include "Bullet/ShooterBulletBase.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"


ShooterBulletManager& ShooterBulletManager::Get()
{
	static ShooterBulletManager Service;
	return Service;
}

void ShooterBulletManager::Reset()
{
	mBulletsPool.Empty();
}

void ShooterBulletManager::ReturnBullet(AShooterBulletBase* Bullet, const uint32 Hash)
{
	TArray<AShooterBulletBase *> * Bullets = mBulletsPool.Find(Hash);
	if (Bullets && Bullet && Bullets->Find(Bullet) == INDEX_NONE)
	{
		Bullet->HideBullet();
		Bullets->Add(Bullet);
	}
}

AShooterBulletBase* ShooterBulletManager::GetBullet(UWorld* World, const TSubclassOf<AShooterBulletBase>& ChildClass)
{
	AShooterBulletBase * FreeBullet = nullptr;
	if (ensure(World))
	{
		const uint32 Hash = GetTypeHash(ChildClass);
		TArray<AShooterBulletBase *> * Bullets = mBulletsPool.Find(Hash);
		if (!Bullets)
		{
			Bullets = &mBulletsPool.Add(Hash, TArray<AShooterBulletBase *>());
		}
		if (Bullets->Num())
		{
			FreeBullet = Bullets->Pop(false);
		}
		else
		{
			FreeBullet = World->SpawnActor<AShooterBulletBase>(ChildClass);
		}

		if (ensure(FreeBullet))
		{
			FreeBullet->DeathEvent.RemoveAll(this);
			FreeBullet->DeathEvent.AddRaw(this, &ShooterBulletManager::ReturnBullet, FreeBullet, Hash);
		}
	}
	return FreeBullet;
}

