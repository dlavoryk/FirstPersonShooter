// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Materials/MaterialExpressionParticleSubUV.h"


class AShooterBulletBase;
/**
 * 
 */
class SHOOTER_API ShooterBulletManager
{

public:
	ShooterBulletManager & operator=(const ShooterBulletManager & Right) = delete;

	static ShooterBulletManager & Get();

	void Reset();

	AShooterBulletBase* GetBullet(UWorld* World, const TSubclassOf<AShooterBulletBase>& ChildClass);


private:
	void ReturnBullet(AShooterBulletBase * Bullet, const uint32 Hash);

	ShooterBulletManager() = default;
	~ShooterBulletManager() = default;
	ShooterBulletManager(const ShooterBulletManager &) = default;
private:
	// pool of bullets. uint -- hash of class 
	UPROPERTY() TMap<uint32, TArray<AShooterBulletBase *>> mBulletsPool;
};
