// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ShooterBulletTrajectoryBase.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER_API UShooterBulletTrajectoryBase : public UObject
{
	GENERATED_BODY()

public:

	void Initialize(const FVector& StartPosition/*, const FVector& FinalPosition*/, const FQuat & ShootingAngleDeg);

	virtual void GetNextLocation(UWorld * World, const float TotalLiveTime, const float DeltaTime, FVector & OutVector,
		const FVector & Velocity);

	virtual void CalculateTrajectory();

protected:
	FVector mStartPosition;
//	FVector mFinalPosition;
	FQuat mShootingOrientation;

	float mVelocityY0;
	float mVelocityX0;

	float mRotationFixX;
	float mRotationFixY;

	float mFlightTime;

	static const float GRAVITY;
};
