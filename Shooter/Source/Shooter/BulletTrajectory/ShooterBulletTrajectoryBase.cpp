// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterBulletTrajectoryBase.h"




const float UShooterBulletTrajectoryBase::GRAVITY = 980.7f;

void UShooterBulletTrajectoryBase::Initialize(const FVector& StartPosition/*, const FVector& FinalPosition*/, const FQuat & ShootingAngleDeg)
{
	mStartPosition = StartPosition;
//	mFinalPosition = FinalPosition;
	mShootingOrientation = ShootingAngleDeg;

	CalculateTrajectory();
}

void UShooterBulletTrajectoryBase::GetNextLocation(UWorld * World, const float TotalLiveTime, const float DeltaTime, FVector & OutVector,
	const FVector & Velocity)
{
	OutVector.X += Velocity.X * DeltaTime;
	OutVector.Y += Velocity.Y * DeltaTime;
	//OutVector.Z += Velocity.Z * DeltaTime;// TotalLiveTime * (HALF_GRAVITY * TotalLiveTime + Velocity.Z);
	OutVector.Z += DeltaTime * (-GRAVITY / 8.0f * TotalLiveTime + Velocity.Z);
}

void UShooterBulletTrajectoryBase::CalculateTrajectory()
{
}
