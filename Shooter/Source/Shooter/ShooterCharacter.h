// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ShooterCharacter.generated.h"


DECLARE_MULTICAST_DELEGATE_OneParam(FOnCReloading, float);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnStopReloading, int, int);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnShooting, int, int);
DECLARE_MULTICAST_DELEGATE_ThreeParams(FOnHealthChanged, int, int, int);


class AShooterWeaponBase;

static int32 MAX_HP = 1000;


UCLASS(config=Game)
class AShooterCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

public:
	AShooterCharacter();

	public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	UFUNCTION(BlueprintCallable, BlueprintPure) bool IsShooting() { return bIsShooting; }

	FORCEINLINE FOnStopReloading & OnStopReloading() { return mOnStopReloading; }
	FORCEINLINE FOnCReloading    & OnReloading    () { return mOnReloading; }
	FORCEINLINE FOnShooting      & OnShooting()      { return mOnShooting; }
	FORCEINLINE FOnHealthChanged & OnHealthChanged() { return mOnHealthChanged; }

	UFUNCTION(BlueprintCallable)FORCEINLINE bool IsAlive() const { return mHP > 0; }
	UFUNCTION(BlueprintCallable)FORCEINLINE int GetHealthLeft() const { return mHP; }

	UFUNCTION(BlueprintCallable) int GetAmmoCount() const;
	UFUNCTION(BlueprintCallable) int GetTotalAmmoCount() const;
	UFUNCTION(BlueprintCallable) float GetReloadingTime() const;

public:
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera) float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera) float BaseLookUpRate;

	UFUNCTION(BlueprintCallable) bool IsJumping() const { return bIsJumping; }

	float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

protected:

	void Tick(float DeltaSeconds) override;
	void ShooterJump();

	void BeginPlay() override;
	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	/** Resets HMD orientation in VR. */
	void OnResetVR();
	/** Called for forwards/backward input */
	void MoveForward(float Value);
	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);


protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	void TakeDamage(int Damage);

private:
	// Configure character movement
	void ConfigureCharacterMovement();

	void AddWeapon();

	void TryFire();
	void StopFire();
	void Fire();


protected:

	UPROPERTY(EditAnywhere, Category = "Movement") FRotator RotationRate = { 0.0f, 540.0f, 0.0f };
	UPROPERTY(EditAnywhere, Category = "Movement") float JumpZVelocity = 600.f;
	UPROPERTY(EditAnywhere, Category = "Weapon|MainWeaponSocket") FString MainWeaponSocketName;

	UPROPERTY(EditAnywhere, Category = "Weapon") TSubclassOf<AShooterWeaponBase> WeaponClass;

	AShooterWeaponBase * MainWeaponComponent = nullptr;

	UPROPERTY(EditAnyWhere, Category = "Weapon") FRotator WeaponRotation = FRotator::ZeroRotator;

private:
	// TODO: CHange to 0?
	float TargetArmLength = 300.f;
	FName AttachedSocketName = NAME_None;// = ;// "root";// """head";

	bool bIsShooting = false;
	bool bIsJumping = false;
	bool bIsReloading = false;


	FOnStopReloading mOnStopReloading;
	FOnCReloading    mOnReloading;
	FOnShooting      mOnShooting;
	FOnHealthChanged mOnHealthChanged;


	int32 mHP = MAX_HP;
};

