// Fill out your copyright notice in the Description page of Project Settings.

#include "MainView.h"


void UMainView::SetHealthBarValue(float Value)
{
	if (ensure(mHealthBar))
	{
		mHealthBar->SetPercent(Value);
	}
}

void UMainView::SetHealthText(const FText& Value)
{
	if (mHealth)
	{
		mHealth->SetText(Value);
	}
}

void UMainView::SetAmmoText(const FString& Text)
{
	if (ensure(mAmmo))
	{
		mAmmo->SetText(FText::FromString(Text));
	}
}

void UMainView::SetAmmoText(const FText& Text)
{
	if (ensure(mAmmo))
	{
		mAmmo->SetText(Text);
	}
}

void UMainView::HideTime()
{
	if (mTime)
	{
		mTime->SetVisibility(ESlateVisibility::Collapsed);
	}
}

void UMainView::SetTime(const FText& Text)
{
	if (mTime)
	{
		mTime->SetVisibility(ESlateVisibility::Visible);
		mTime->SetText(Text);
	}
}

void UMainView::Initwidgets(UProgressBar* HealthBar, UTextBlock* AmmoBlock, UTextBlock * Time, UTextBlock * HealthBlock)
{
	ensure(HealthBar);
	ensure(AmmoBlock);
	ensure(Time);
	ensure(HealthBlock);
	mHealthBar = HealthBar;
	mAmmo      = AmmoBlock;
	mTime      = Time;
	mHealth    = HealthBlock;
}
