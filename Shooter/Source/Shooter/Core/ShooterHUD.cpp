// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterHUD.h"
#include "UI/MainView.h"
#include "ShooterCharacter.h"


void AShooterHUD::BeginPlay()
{
	Super::BeginPlay();

	mMainView = CreateWidget<UMainView>(GetOwningPlayerController(), MainViewClass);
	mMainView->AddToViewport(1);

	/// cOULD BE FIXED WITH GAME_INSTANCE(?)

	if (auto OwningPlayer = Cast<AShooterCharacter>(GetOwningPawn()))
	{
		OwningPlayer->OnReloading().AddLambda([this](float Time)
		{
			SetTime(FText::AsNumber(Time));
		});

		OwningPlayer->OnStopReloading().AddLambda([this](int MagAmmo, int TotalAmmo)
		{
			SetAmmo(FText::FromString(FString::Printf(TEXT("%i/%i"), MagAmmo, TotalAmmo)));
			StopTime();
		});

		OwningPlayer->OnShooting().AddLambda([this](int MagAmmo, int TotalAmmo)
		{
			SetAmmo(FText::FromString(FString::Printf(TEXT("%i / %i"), MagAmmo, TotalAmmo)));
		});
		OwningPlayer->OnHealthChanged().AddLambda([this](int HealthLeft, int MaxHealth, int Damage)
		{
			SetHealth((float)HealthLeft / (float)MaxHealth);
			SetHealth(FText::FromString(FString::Printf(TEXT("%i / %i"), HealthLeft, MaxHealth)));
		});


		SetAmmo(FText::FromString(FString::Printf(TEXT("%i/%i"), OwningPlayer->GetAmmoCount(), OwningPlayer->GetTotalAmmoCount())));

		SetHealth(1);
		SetHealth(FText::FromString(FString::Printf(TEXT("%i / %i"), 100, 100)));
	}
}

void AShooterHUD::SetHealth(FText&& HealthText)
{
	if (mMainView)
	{
		mMainView->SetHealthText(HealthText);
	}
}

void AShooterHUD::SetHealth(float HealthPercentage)
{
	if (mMainView)
	{
		mMainView->SetHealthBarValue(HealthPercentage);
	}
}

void AShooterHUD::SetAmmo(FText&& AmmoText)
{
	if (mMainView)
	{
		mMainView->SetAmmoText(AmmoText);
	}
}

void AShooterHUD::SetTime(FText&& TimeText)
{
	if (mMainView)
	{
		mMainView->SetTime(TimeText);
	}
}

void AShooterHUD::StopTime()
{
	if (mMainView)
	{
		mMainView->HideTime();
	}
}