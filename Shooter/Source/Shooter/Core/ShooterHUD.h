// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "ShooterHUD.generated.h"

class UMainView;
/**
 * 
 */
UCLASS()
class SHOOTER_API AShooterHUD : public AHUD
{
	GENERATED_BODY()


public:
	void BeginPlay() override;

	void SetHealth(FText && HealthText);
	void SetHealth(float HealthPercentage);
	void SetAmmo(FText && AmmoText);
	void SetTime(FText && TimeText);
	void StopTime();

protected:
	UPROPERTY(EditAnyWhere)
	TSubclassOf<UMainView> MainViewClass;

private:
	UMainView * mMainView = nullptr;
	
};
