// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/World.h"

#include "Weapon/ShooterWeaponBase.h"

//////////////////////////////////////////////////////////////////////////
// AShooterCharacter

AShooterCharacter::AShooterCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate   = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw   = false;
	bUseControllerRotationRoll  = false;

	// Configure character movement
//	ConfigureCharacterMovement();

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
//	CameraBoom->SetupAttachment(Cast<USceneComponent>(GetMesh()), AttachedSocketName);
	CameraBoom->SetupAttachment(GetRootComponent(), AttachedSocketName);

	CameraBoom->TargetArmLength = TargetArmLength; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
}

int AShooterCharacter::GetAmmoCount() const
{
	return MainWeaponComponent ? MainWeaponComponent->GetAmmoInMag() : 0;
}

int AShooterCharacter::GetTotalAmmoCount() const
{
	return MainWeaponComponent ? MainWeaponComponent->GetTotalAmmo() : 0;
}

float AShooterCharacter::GetReloadingTime() const
{
	return 0;
//	return MainWeaponComponent ? MainWeaponComponent->getreloa : 0;
}

//////////////////////////////////////////////////////////////////////////
// Input

void AShooterCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AShooterCharacter::TryFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &AShooterCharacter::StopFire);


	PlayerInputComponent->BindAxis("MoveForward", this, &AShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AShooterCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AShooterCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AShooterCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AShooterCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AShooterCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AShooterCharacter::OnResetVR);
}

void AShooterCharacter::TakeDamage(int Damage)
{
	mHP -= Damage;
	if (mHP < 0)
	{
		mHP = 0;
		// Die();
	}
}

void AShooterCharacter::ConfigureCharacterMovement()
{
	GetCharacterMovement()->bUseControllerDesiredRotation = true;
//	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = RotationRate; // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = JumpZVelocity;
	GetCharacterMovement()->AirControl = 0.2f;
}

void AShooterCharacter::AddWeapon()
{
	 FActorSpawnParameters SpawnParams;
	 SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	 AShooterWeaponBase* NewWeaponComponent = GetWorld()->SpawnActor<AShooterWeaponBase>(WeaponClass, FVector::ZeroVector, WeaponRotation, SpawnParams);

	 if (NewWeaponComponent)
	 {
		 MainWeaponComponent = NewWeaponComponent;
		 MainWeaponComponent->AttachToParent(GetMesh(), MainWeaponSocketName);

		 MainWeaponComponent->OnReloading.BindLambda([this](float Time, bool IsReloading)
		 {
			 bIsReloading = IsReloading;
			 if (bIsReloading)
			 {
				 mOnReloading.Broadcast(Time);
			 }
			 else
			 {
				 mOnStopReloading.Broadcast(GetAmmoCount(), GetTotalAmmoCount());
			 }
		 });
	 }

}

void AShooterCharacter::ShooterJump()
{
	bIsJumping = true;
	Jump();
}

void AShooterCharacter::TryFire()
{
	bIsShooting = true;
}

void AShooterCharacter::StopFire()
{
	bIsShooting = false;
}

void AShooterCharacter::Fire()
{
	if (MainWeaponComponent)
	{
		if (MainWeaponComponent->Fire(GetVelocity()))// && bIsImmortal)
		{
			mOnShooting.Broadcast(GetAmmoCount(), GetTotalAmmoCount());
		//	BecomeMortal();
		}
	}
}


float AShooterCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	int Dmg = 0;
	if (IsAlive())
	{
		TakeDamage(DamageAmount);
		OnHealthChanged().Broadcast(GetHealthLeft(), MAX_HP, DamageAmount);
	}
	return Dmg;
}

void AShooterCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (bIsShooting)
	{	
		Fire();
	}
}

void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();
	ConfigureCharacterMovement();

	AddWeapon();
	OnHealthChanged().Broadcast(GetHealthLeft(), MAX_HP, 0);

}

void AShooterCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

void AShooterCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AShooterCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void AShooterCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void AShooterCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AShooterCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
		
		/*
		const FRotator Rotation = Controller->GetControlRotation();
		FRotator NewRotator(ForceInit);
		NewRotator.Yaw = Rotation.Yaw;
		AddMovementInput(NewRotator.Vector() * FVector::RightVector, Value);*/
	}
}
