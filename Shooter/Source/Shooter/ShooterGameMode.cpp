// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ShooterGameMode.h"
#include "ShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Services/ShooterBulletManager.h"

AShooterGameMode::AShooterGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Player/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void AShooterGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	ShooterBulletManager::Get().Reset();
	Super::EndPlay(EndPlayReason);
}
