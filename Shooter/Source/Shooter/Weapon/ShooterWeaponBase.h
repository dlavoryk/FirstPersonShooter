// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ShooterWeaponBase.generated.h"

class UAudioComponent;
class USoundCue;
class AShooterBulletBase;

DECLARE_DELEGATE_TwoParams(FOnReloading, const float, const bool)

UCLASS()
class SHOOTER_API AShooterWeaponBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShooterWeaponBase();


	virtual void AttachToParent(USkeletalMeshComponent* ParentMeshComponent, FString SocketName);

	virtual bool Fire(const FVector & StartVelocity);

	void AddAmmo(int32 NewAmmo);
	void BeginReload();

	int32 GetAmmoInMag() const;
	int32 GetTotalAmmo() const;
	int32 GetMaxAmmoInMang() const;


	FOnReloading OnReloading;

protected:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MeshCoupling();

	virtual void UpdateSound(float DeltaTime);
	virtual void UpdateReload(float DeltaTime);

	UFUNCTION(BlueprintCallable) void Initiailize(USkeletalMeshComponent* SkeletalMesh);

private:
	void ReloadMag();
	void FireInternal(const FVector & StartVelocity);

protected:
	FString RootBoneName;
	FName FireSocketName;


	UPROPERTY(EditAnywhere, BlueprintReadOnly) float mRotationSpeedDegreesPerSec;
	UPROPERTY(EditAnywhere, BlueprintReadOnly) float mReloadDelay = 1.f;
	UPROPERTY(EditAnyWhere, BlueprintReadOnly) int32 MaxMagAmmoCount = 30;
	UPROPERTY(EditAnyWhere, BlueprintReadOnly) float FireRate = 1;
	UPROPERTY(EditAnyWhere, BlueprintReadOnly) int32 BulletSpeed = 0;
	UPROPERTY(EditAnyWhere, BlueprintReadOnly) TSubclassOf<AShooterBulletBase> BulletClass;
	UPROPERTY(EditAnywhere, BlueprintReadOnly) float Damage = 0.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadOnly) int32 mTotalAmmoCount;


	/** max length of an ray tracing */
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true")) float MaxRayCastLengthCm;
	/** weapon shoot sound stop */
	UPROPERTY(Category = Audio, EditDefaultsOnly) USoundCue* ShootSound;
	/** audio component for weapon sounds */
	UPROPERTY(Category = Audio, BlueprintReadOnly, meta = (AllowPrivateAccess = "true")) UAudioComponent* WeaponAC;
	/** shoting audion delay */
	UPROPERTY(Category = Audio, BlueprintReadOnly, meta = (AllowPrivateAccess = "true")) float BaseShootingSoundDelay;

	int32 mMagAmmoCount = 0;

	float mShootWaitingTime;

	float mWaitingTime = 0.f;
	float mRelodWaitingTime = 0.f;

	bool bIsReadyToFire = true;
	bool bIsReloading = false;

	bool bIsShootingSoundPlay = false;
	float mTimeToSoundStop = 0.0f;


	class USkeletalMeshSocket const*  mFireSocket;


	FString ParentSocketName;
	USkeletalMeshComponent* ParentComponent;
	USkeletalMeshComponent* SkeletalMesh;


	
	
};
