// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterWeaponBase.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/AudioComponent.h"
#include "Bullet/ShooterBulletBase.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Services/ShooterBulletManager.h"
#include "ShooterCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

// Sets default values
AShooterWeaponBase::AShooterWeaponBase()
{
	PrimaryActorTick.bCanEverTick = true;

	// ...
	RootBoneName = "Root";
	FireSocketName = "Fire";// "FireSocket";
	mRotationSpeedDegreesPerSec = 180.0f;

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PostPhysics;

	WeaponAC = CreateDefaultSubobject<UAudioComponent>(TEXT("WeaponAudio"));
	WeaponAC->SetupAttachment(RootComponent);

	BaseShootingSoundDelay = 0.2f;
	MaxRayCastLengthCm = 1000.0f;

	//mAimStartOrientation = FQuat::Identity;

}

void AShooterWeaponBase::AttachToParent(USkeletalMeshComponent* ParentMeshComponent, FString SocketName)
{
	if (ParentMeshComponent != nullptr)
	{
		ParentSocketName = SocketName;
		ParentComponent = ParentMeshComponent;
		MeshCoupling();
	}
}

bool AShooterWeaponBase::Fire(const FVector& StartVelocity)
{
	bool Res = false;
	if (!bIsReloading)
	{
		if (mMagAmmoCount <= 0)
		{
			mMagAmmoCount = 0;
			BeginReload();
		}
		else if (bIsReadyToFire)
		{
			Res = true;
			FireInternal(StartVelocity);
		}
	}
	return Res;
}

// Called when the game starts or when spawned
void AShooterWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	mFireSocket = SkeletalMesh->GetSocketByName(FireSocketName);
	mShootWaitingTime = 1.f / FireRate;
	//	mMagAmmoCount = MaxMagAmmoCount;
	BeginReload();
	if (WeaponAC)
	{
//		WeaponAC->SetSound(ShootSound);
//		WeaponAC->Stop();
	}
}

int32 AShooterWeaponBase::GetMaxAmmoInMang() const
{
	return MaxMagAmmoCount;
}

void AShooterWeaponBase::MeshCoupling()
{
	if (SkeletalMesh != nullptr && ParentComponent != nullptr)
	{
		AttachToComponent(ParentComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale, FName(*ParentSocketName));
	//	AttachToComponent(ParentComponent, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), FName(*ParentSocketName));

//		AttachToActor(ParentComponent->GetAttachmentRootActor(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, FName(*ParentSocketName));
	//	RootComponent->bAbsoluteRotation = true;
	}
}

void AShooterWeaponBase::Initiailize(USkeletalMeshComponent* SkeletalMesh)
{
	this->SkeletalMesh = SkeletalMesh;
}

void AShooterWeaponBase::ReloadMag()
{
	const int32 NeededAmmo = MaxMagAmmoCount - mMagAmmoCount;
	mTotalAmmoCount -= NeededAmmo;

	mMagAmmoCount = MaxMagAmmoCount;
	if (mTotalAmmoCount < 0)
	{
		mMagAmmoCount += mTotalAmmoCount;
		mTotalAmmoCount = 0;
	}
}

void AShooterWeaponBase::FireInternal(const FVector& StartVelocity)
{
	--mMagAmmoCount;
	AShooterBulletBase * Bullet = ShooterBulletManager::Get().GetBullet(GetWorld(), BulletClass);
	if (Bullet && mFireSocket)
	{

/*		const FVector SocketPosition = mFireSocket->GetSocketLocation(SkeletalMesh);
		const FTransform & Transform = mFireSocket->GetSocketTransform(SkeletalMesh);// .Rotator().Quaternion();


		const FQuat Quat = Transform.Rotator().Quaternion();
		const FVector ForwardVector = Quat * FVector::ForwardVector;// GetActorRotation().Quaternion() * FVector::ForwardVector;
		
		Bullet->Shoot(SocketPosition, Quat, ForwardVector.GetSafeNormal() * BulletSpeed + StartVelocity, Damage, GetAttachParentActor());
		*/

		auto Shooter = Cast<AShooterCharacter>(GetAttachParentActor());
		auto PlayerController = Cast<APlayerController>(Shooter->GetController());
		FVector FinalRotation, FinalLocation;
	//	PlayerController->DeprojectScreenPositionToWorld(0, 0, FinalRotation, FinalLocation);


		/*FRotator Loc;
		PlayerController->GetActorEyesViewPoint(FinalLocation, Loc);
		const FVector SocketPosition = mFireSocket->GetSocketLocation(SkeletalMesh);
		Bullet->Shoot(SocketPosition, FinalRotation.ToOrientationQuat(), Loc.Vector().GetSafeNormal() * BulletSpeed + StartVelocity, Damage, GetAttachParentActor());
		*/
	/*	FMinimalViewInfo Info;
		Shooter->GetFollowCamera()->GetCameraView(0, Info);
		const FVector SocketPosition = mFireSocket->GetSocketLocation(SkeletalMesh);
		Bullet->Shoot(SocketPosition, FinalRotation.ToOrientationQuat(), Info.Rotation.Vector().GetSafeNormal() * BulletSpeed + StartVelocity, Damage, GetAttachParentActor());
*/
		
		const FVector SocketPosition = mFireSocket->GetSocketLocation(SkeletalMesh);
		FVector CameraForwardvector = Shooter->GetFollowCamera()->GetForwardVector();

		Bullet->Shoot(SocketPosition, CameraForwardvector.ToOrientationQuat(), CameraForwardvector * BulletSpeed + StartVelocity, Damage, GetAttachParentActor());

		

//		Bullet->Shoot(SocketPosition, FinalRotation.ToOrientationQuat(), Shooter->GetFollowCamera()->GetForwardVector().GetSafeNormal() * BulletSpeed + StartVelocity, Damage, GetAttachParentActor());


		bIsShootingSoundPlay = true;
	}

	bIsReadyToFire = false;
}


void AShooterWeaponBase::UpdateSound(float DeltaTime)
{
	/*
	if (WeaponAC == nullptr)
	{
		return;
	}

	if (bIsShootingSoundPlay)
	{
		mTimeToSoundStop = BaseShootingSoundDelay;
		WeaponAC->SetBoolParameter("IsActive", true);

		if (!WeaponAC->IsPlaying())
		{
			WeaponAC->Play();
		}

		bIsShootingSoundPlay = false;
	}
	else
	{
		mTimeToSoundStop -= DeltaTime;
		if (mTimeToSoundStop <= 0.0f)
		{
			WeaponAC->SetBoolParameter("IsActive", false);
		}
	}*/
}


void AShooterWeaponBase::UpdateReload(float DeltaTime)
{
	if (!bIsReadyToFire)
	{
		mWaitingTime += DeltaTime;
		if (mWaitingTime >= mShootWaitingTime)
		{
			mWaitingTime = 0.f;
			bIsReadyToFire = true;
		}
	}

	if (bIsReloading)
	{
		mRelodWaitingTime += DeltaTime;
		if (mRelodWaitingTime >= mReloadDelay)
		{
			mRelodWaitingTime = 0.f;
			bIsReloading = false;
			ReloadMag();
		}
		OnReloading.ExecuteIfBound(mReloadDelay - mRelodWaitingTime, bIsReloading);
	}
}

void AShooterWeaponBase::AddAmmo(int32 NewAmmo)
{
	mTotalAmmoCount += NewAmmo;
	if (mMagAmmoCount <= 0)
	{
		BeginReload();
	}
}

void AShooterWeaponBase::BeginReload()
{
	if (mTotalAmmoCount > 0 && mMagAmmoCount != MaxMagAmmoCount)
	{
		bIsReloading = true;
	}
}

int32 AShooterWeaponBase::GetAmmoInMag() const
{
	return mMagAmmoCount;
}

int32 AShooterWeaponBase::GetTotalAmmo() const
{
	return mTotalAmmoCount;
}

// Called every frame
void AShooterWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!HasActorBegunPlay())
	{
		return;
	}
	UpdateReload(DeltaTime);
	UpdateSound(DeltaTime);
}