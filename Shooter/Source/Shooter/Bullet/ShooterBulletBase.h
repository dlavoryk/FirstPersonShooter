// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ShooterBulletBase.generated.h"

class UParticleSystemComponent;
DECLARE_MULTICAST_DELEGATE(FOnDeath)
class UShooterBulletTrajectoryBase;
class UNiXBulletFX;

UCLASS()
class SHOOTER_API AShooterBulletBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShooterBulletBase();
	AShooterBulletBase(const FObjectInitializer& ObjectInitializer);

	virtual bool CheckCollisions(const FVector & Point1, const FVector & Point2);//. const;

	void HideBullet();

	void SetVelocity(const FVector & NewVelocity);

	void Shoot(const FVector & StartPosition/*, const FVector & FinalPosition*/, const FQuat & WeaponOrientation, const FVector & NewVelocity,
		const float Damage, AActor * Shooter);


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void PlayFX(TWeakObjectPtr<class UPhysicalMaterial> ContactSurface);


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
//	UPROPERTY(EditAnywhere, Category = "Settings", DisplayName = "BulletFX Type") UNiXBulletFX* BulletFX_Type;
	UPROPERTY(EditAnywhere, Category = "Settings") TSubclassOf<UShooterBulletTrajectoryBase> MovementClass;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Settings") float MaxLivingTime = 10.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Settings") float DamageDistance = 0.f;

public:
	FOnDeath DeathEvent;

private:
	AActor * mShooter;

	float mDamage = 0.0f;
	float mLifeTime = 0.0f;

	UShooterBulletTrajectoryBase * mMovementController;
	UParticleSystemComponent * mPSComponent;

	FVector mVelocity = FVector::ZeroVector;
	FVector mCurLocation = FVector::ZeroVector;
	
	
};
