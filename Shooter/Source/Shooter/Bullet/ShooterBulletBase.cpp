// Fill out your copyright notice in the Description page of Project Settings.

#include "ShooterBulletBase.h"
#include "Kismet/GameplayStatics.h"
#include "BulletTrajectory/ShooterBulletTrajectoryBase.h"
#include "Engine/World.h"

// Sets default values
AShooterBulletBase::AShooterBulletBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	HideBullet();
}

AShooterBulletBase::AShooterBulletBase(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = true;
	HideBullet();
}

bool AShooterBulletBase::CheckCollisions(const FVector & Point1, const FVector & Point2)// const
{
	FHitResult OutHit;
	FCollisionQueryParams Params = FCollisionQueryParams::DefaultQueryParam;
	Params.bReturnPhysicalMaterial = true;
	
	const bool IsHit = GetWorld()->LineTraceSingleByChannel(OutHit, Point1, Point2, ECollisionChannel::ECC_Pawn, Params);
	if (IsHit)
	{
		if (DamageDistance == 0.0f)
		{
		//	AShooter * HitActor = Cast<ANiXVehiclePawn>(OutHit.GetActor());
		//	if (HitActor && mShooter != HitActor)
		//	{
		//		HitActor->TakeDamage(mDamage, FDamageEvent(), nullptr, mShooter);
		//	}
			OutHit.GetActor()->TakeDamage(mDamage, FDamageEvent(), nullptr, mShooter);
		}
		else
		{
		//	TArray<AActor*> DamagedActors;
		//	UGameplayStatics::GetAllActorsOfClass(this, APawn::StaticClass(), DamagedActors);
		//	float DamageDistanceSqr = (DamageDistance * DamageDistance);
		//	for (int i = 0; i < DamagedActors.Num(); i++)
		//	{
		//		ANiXVehiclePawn * HitActor = Cast<ANiXVehiclePawn>(DamagedActors[i]);
		//		if (HitActor && mShooter != HitActor)
		//		{
		//			float cDistanceSqr = FVector::DistSquared(HitActor->GetActorLocation(), GetActorLocation());
		//			if (cDistanceSqr < DamageDistanceSqr)
		//			{
		//				float DistanceDamage = mDamage * (1.0f - cDistanceSqr / DamageDistanceSqr);
		//				HitActor->TakeDamage(FMath::RoundHalfFromZero(DistanceDamage), FDamageEvent(), nullptr, mShooter);
		//			}
		//		}
		//	}
		}

		PlayFX(OutHit.PhysMaterial);
		DeathEvent.Broadcast();
	}

	return IsHit;
}

void AShooterBulletBase::SetVelocity(const FVector & NewVelocity)
{
	mVelocity = NewVelocity;
}

void AShooterBulletBase::HideBullet()
{
	/*
	if (PSComponent != nullptr && PSComponent->IsValidLowLevel())
	{
		PSComponent->RemoveFromRoot();
		PSComponent->Deactivate();

		PSComponent->DestroyComponent(true);
		PSComponent = nullptr;
	}*/

	SetActorHiddenInGame(true);
	SetActorTickEnabled(false);
	SetVelocity(FVector::ZeroVector);
}

void AShooterBulletBase::Shoot(const FVector & StartPosition/*, const FVector & FinalPosition*/, const FQuat & WeaponOrientation, const FVector & NewVelocity,
	const float Damage, AActor * Shooter)
{
	mDamage = Damage;
	mLifeTime = 0.f;
	mCurLocation = StartPosition;
	SetActorLocation(StartPosition);
	SetVelocity(NewVelocity);
	SetActorTickEnabled(true);
	SetActorHiddenInGame(false);
	mShooter = Shooter;
//	mShooter = Cast<ANiXVehiclePawn>(Shooter);
//	ensure(mShooter);

	if (mMovementController)
	{
		mMovementController->Initialize(StartPosition/*, FinalPosition*/, WeaponOrientation);
	}

	// Get FX for bullet trail
//	UParticleSystem* TrailFX = (BulletFX_Type ? BulletFX_Type->GetTrailFX() : nullptr);
//	if (TrailFX)
//	{
//		mPSComponent = UGameplayStatics::SpawnEmitterAttached(TrailFX, RootComponent, *RootComponent->GetName(), FVector::ZeroVector, GetActorRotation());
//	}
}

// Called when the game starts or when spawned
void AShooterBulletBase::BeginPlay()
{
	Super::BeginPlay();
	if (MovementClass != nullptr)
	{
		mMovementController = NewObject<UShooterBulletTrajectoryBase>(this, MovementClass);
	}
	else
	{
		mMovementController = NewObject<UShooterBulletTrajectoryBase>();
	}
	mMovementController->AddToRoot();
}

void AShooterBulletBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (mMovementController != nullptr)
	{
		mMovementController->RemoveFromRoot();
	}
	Super::EndPlay(EndPlayReason);
}

void AShooterBulletBase::PlayFX(TWeakObjectPtr<class UPhysicalMaterial> ContactSurface)
{
	/*if (ContactSurface.Get() && BulletFX_Type)
	{
		UParticleSystem* HitFX = BulletFX_Type->GetSurfaceHitFX(ContactSurface->SurfaceType);
		if (HitFX)
		{
			//UGameplayStatics::SpawnEmitterAttached(HitFX, RootComponent, *RootComponent->GetName(), FVector::ZeroVector, GetActorRotation());
			UGameplayStatics::SpawnEmitterAtLocation(this, HitFX, GetActorLocation(), GetActorRotation());
		}
		USoundCue* SoundQueue = BulletFX_Type->GetImpactSound(ContactSurface->SurfaceType);
		if (SoundQueue)
		{
			UGameplayStatics::PlaySoundAtLocation(this, SoundQueue, GetActorLocation());
		}
	}*/
}

// Called every frame
void AShooterBulletBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	mLifeTime += DeltaTime;
	if (mLifeTime >= MaxLivingTime)
	{
		DeathEvent.Broadcast();
	}
	else
	{
		mMovementController->GetNextLocation(GetWorld(), mLifeTime, DeltaTime, mCurLocation, mVelocity);

		if (!CheckCollisions(GetActorLocation(), mCurLocation))
		{
			const FVector LookAtAxis = GetActorLocation() - mCurLocation;
			const FQuat LookAtOrientation = FRotationMatrix::MakeFromX(LookAtAxis).ToQuat();

			SetActorLocation(mCurLocation);
			SetActorRotation(LookAtOrientation);
		}
		else
		{
			mLifeTime = 0.0f;
		}
	}
}

